#!/bin/sh
hostmaster="hostmaster@test.ru"      # Электропочта Администратора сервера
www_path="/var/www/hosting/"        # Путь до директории с виртуальными хостами
wwwuser="www-data"                
wwwgroup="www-data"
 
case "$@" in
    "")
        echo "имя домена (as root)."
        ;;
    *)
        clear
        echo "Создаю директории сайта"
        mkdir -p $www_path$1/www/
        mkdir -p $www_path$1/cgi-bin/
        mkdir -p $www_path$1/log/
        echo "$www_path$1/www/"
        echo "$www_path$1/cgi-bin/"
        echo "$www_path$1/log/"
 
        echo "\nСоздаю пустой index.html "
        echo " " > $www_path$1/www/index.html
        chown -R $wwwuser:$wwwgroup /$www_path$1
        chmod -R 0755 /$www_path$1
        echo "\nДобавляю хост в: /etc/apache2/sites-enabled/$1"
        exec 3>&1 1>/etc/apache2/sites-enabled/$1
        echo "<virtualhost \${HOSTING_HOST}:$2>"
        echo "	ServerName $1"
        echo "	ServerAdmin $hostmaster"
        echo "	"
        echo "	DocumentRoot \${HOSTING_ROOT}/$1/www/"
        echo "	<Directory>"
        echo "		Options Indexes Includes FollowSymLinks MultiViews"
	echo "		Order allow,deny"
	echo "		AllowOverride All"
        echo "		Allow from All"
	echo "	</Directory>"
        echo "	"
	echo "	<Directory \${HOSTING_ROOT}$1/www/>"
    	echo "		Options Indexes Includes FollowSymLinks MultiViews"
        echo "		Order allow,deny"
    	echo "		AllowOverride All"
    	echo "		Allow from All"
        echo "	</Directory>"
        echo "	"
 	echo "	ScriptAlias /cgi-bin/ \${HOSTING_ROOT}/$1/cgi-bin/"
        echo "	<Directory \${HOSTING_ROOT}/$1/cgi-bin/>"
    	echo "		AllowOverride None"
    	echo "		Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch"
    	echo "		Order allow,deny"
    	echo "		Allow from all"
        echo "	</Directory>"
        echo "	"
    	echo "	ErrorLog \${HOSTING_ROOT}/$1/log/error.log"
  	echo "	LogLevel warn"
    	echo "	CustomLog \${HOSTING_ROOT}/$1/log/access.log combined"
    	echo "	ServerSignature On"
	echo "	"
        echo "</virtualhost>"
        exec 1>&3

        echo "\nДобавляю хост в: /etc/nginx/sites-enabled/$1"
        exec 3>&1 1>/etc/nginx/sites-enabled/$1
        echo "server {"
	echo "	listen 80;"
	echo "	server_name $1;"
	echo "	"
    	echo "	#charset koi8-r;"
      	echo "	"
    	echo "	access_log $www_path$1/log/$1-nginx.access.log main;"
        echo "	"       
    	echo "	location / {"
    	echo "		proxy_pass http://127.0.0.1:$2/;"
    	echo "		proxy_redirect off;"
    	echo "		proxy_set_header Host \$host;"
    	echo "		proxy_set_header X-Real-IP \$remote_addr;"
    	echo "		proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;"
    	echo "		client_max_body_size 40m;"
    	echo "		client_body_buffer_size 256k;"
        echo "		"       			          
    	echo "		proxy_connect_timeout 120;"
    	echo "		proxy_send_timeout 120;"
    	echo "		proxy_read_timeout 120;"
    	echo "		proxy_buffer_size 64k;"
    	echo "		proxy_buffers 4 64k;"
    	echo "		proxy_busy_buffers_size 64k;"
    	echo "		proxy_temp_file_write_size 64k;"
    	echo "		proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;"
    	echo "	}"
    	echo "	#Static files location"
    	echo "	location ~* ^.+.(jpg|jpeg|gif|png|ico|css|zip|tgz|gz|rar|bz2|doc|xls|exe|pdf|ppt|txt|tar|mid|midi|wav|bmp|rtf|js|html|flv|mp3)$ "
    	echo "	{"
     	echo "	root $www_path$1/www/;"
    	echo "}"
		echo "}"
        exec 1>&3

        sleep 1
        echo "Перезапуск вэбсервера"
        sudo /etc/init.d/apache2 restart
        sudo /etc/init.d/nginx restart
        echo "Домен создан;)"
        echo "Теперь можно перейти по адресу http://$1"
        ;;
esac
