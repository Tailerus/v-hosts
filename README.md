# V-hosts

Конфигурация следующая:
Стандартный LAMP + NGINX в качестве фронт-энда.
Директория с хостами /var/www/hosting/
Директоря виртуального хоста = имени домена
Стуктура виртуального хоста
test.ru/cgi-bin
test.ru/www
test.ru/log


Порты перечислены в конфиге /etc/apache2/ports.conf <details><summary>Click to expand</summary>
<br>Listen 8080 
<br>Listen 8081 
<br>Listen 8082 
<br>Listen 8083 
</details>

${HOSTING_HOST} и ${HOSTING_ROOT} — переменные Apache2

${HOSTING_ROOT} имеет то же значение что и $www_path


sudo ./addvhost.sh test.ru 8080
где «test.ru» — имя нашего домена (присваивается $1), а «8080» — порт на котором у нас будет сидеть этот тестовый домен (присваивается $2).
